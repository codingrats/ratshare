from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.template.loader import render_to_string
from django.http import JsonResponse
from files.forms import FileForm
from files.models import FileUpload
from files.decorators import user_is_file_author
from django.contrib import messages
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage


@login_required
def index(request):
    template = "files/file-list.html"
    file_list = FileUpload.objects.filter(user_id=request.user.id, in_trash=False).order_by('-upload_date')
    count = FileUpload.objects.filter(user_id=request.user.id, in_trash=True).count()
    paginator = Paginator(file_list, 10)

    page = request.GET.get('page', 1)
    try:
        files = paginator.page(page)
    except PageNotAnInteger:
        files = paginator.page(1)
    except EmptyPage:
        files = paginator.page(paginator.num_pages)

    if not file_list:
        messages.info(request, 'File list is empty')

    doc = ['doc', 'docx', 'ppt', 'pptx', 'xls', 'xlsx']
    audio = ['mp3']
    images = ['jpg', 'bmp', 'gif', 'png']
    source_code = ['css', 'html', 'php', 'cpp', 'py', 'cs', 'xml', 'json', 'md', 'ini', 'rb', 'h', 'sql', 'js',
                   'java', 'txt', 'log']
    context = {'file_list': file_list, 'doc': doc, 'audio': audio, 'images': images, 'source_code': source_code, 'count': count,
               'files': files}
    return render(request, template, context)


@login_required
def file_upload(request):
    source_code = ['css', 'html', 'php', 'cpp', 'py', 'cs', 'xml', 'json', 'md', 'ini', 'rb', 'h', 'sql', 'js', 'java',
                   'txt', 'log']
    name = ''
    file_type = ''
    if request.method == 'POST':
        form = FileForm(request.POST, request.FILES)
        my_upload = request.FILES['file'].read()
        for filename, file in request.FILES.items():
            name = request.FILES[filename].name
            file_type = name.split('.')[-1]

        if form.is_valid():
            form.instance.name = name
            form.instance.file_type = file_type
            file = form.save(commit=False)
            file.user_id = request.user

            if file_type in source_code:
                form.instance.source_code = my_upload
            file.save()

        else:
            form = FileForm()
        return render(request, 'files/file-list.html', locals())


@login_required
def remove_file(request, pk):
    file = FileUpload.objects.get(pk=pk)
    file.in_trash = True
    file.save()
    messages.success(request, 'File {0} moved to trash successfully'.format(file.name))
    return redirect('/files/')


@login_required
@user_is_file_author
def delete_file(request, pk):
    file = FileUpload.objects.get(pk=pk)
    file.file.delete()
    file.delete()
    messages.warning(request, 'File {0} successfully deleted'.format(file.name))
    return redirect('/files/trashlist/')


@login_required
@user_is_file_author
def return_file(request, pk):
    file = FileUpload.objects.get(pk=pk)
    file.in_trash = False
    file.save()
    messages.success(request, 'File {0} successfully moved back to file list'.format(file.name))
    return redirect('/files/trashlist/')


def trash_list(request):
    data = dict()
    files = FileUpload.objects.filter(user_id=request.user.id, in_trash=True)

    if not files:
        messages.info(request, 'Trash list is empty')

    context = {'files': files}
    data['html_table'] = render_to_string('files/trash-list.html', context, request)
    return JsonResponse(data)
