from django.core.exceptions import PermissionDenied
from files.models import FileUpload


def user_is_file_author(function):
    def wrap(request, *args, **kwargs):
        file = FileUpload.objects.get(pk=kwargs['pk'])
        if file.user_id == request.user and file.in_trash:
            return function(request, *args, **kwargs)
        raise PermissionDenied
    return wrap
