# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-08-04 13:42
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('files', '0003_auto_20170802_2033'),
    ]

    operations = [
        migrations.AddField(
            model_name='fileupload',
            name='in_trash',
            field=models.BooleanField(default=False),
        ),
    ]
