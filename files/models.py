from django.db import models
from django.contrib.auth.models import User


def get_user_file_folder():
    # return 'files/{0}{1}'.format(instance.user.username, filename)
    return 'files/'


class FileUpload(models.Model):
    user_id = models.ForeignKey(User, null=True, blank=True)
    name = models.CharField(max_length=255, blank=True)
    file_type = models.CharField(max_length=255, blank=True)
    file_size = models.CharField(max_length=255, blank=True)
    file = models.FileField(upload_to='files/')
    source_code = models.TextField(max_length=2000, blank=True, null=True)
    upload_date = models.DateTimeField(auto_now_add=True)
    in_trash = models.BooleanField(default=False)
