from django.conf.urls import url
from . import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^upload/$', views.file_upload, name='upload'),
    url(r'^(?P<pk>[0-9]+)/removefile/$', views.remove_file, name='removefile'),
    url(r'^(?P<pk>[0-9]+)/deletefile/$', views.delete_file, name='deletefile'),
    url(r'^(?P<pk>[0-9]+)/returnfile/$', views.return_file, name='returnfile'),
    url(r'^trashlist/$', views.trash_list, name='trashlist')
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
