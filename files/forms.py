from files.models import FileUpload
from django.forms import ModelForm


class FileForm(ModelForm):
    class Meta:
        model = FileUpload
        fields = ['file']