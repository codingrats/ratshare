from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit, Layout, Field, Button
from django.forms import ModelForm
from notes.models import Note


class NoteForm(ModelForm):
    helper = FormHelper()
    layout = helper.layout = Layout()
    layout.append(Field('title', placeholder='Title'))
    layout.append(Field('content', placeholder='Description'))
    layout.append(Field('status'))
    helper.form_tag = False
    helper.form_show_labels = False
    helper.add_input(Submit('submit', 'Submit'))

    class Meta:
        model = Note
        exclude = ['id', 'user_id', 'edit_date']
