from django.core.exceptions import PermissionDenied
from notes.models import Note


def user_is_note_author(function):
    def wrap(request, *args, **kwargs):
        note = Note.objects.get(pk=kwargs['pk'])
        if note.user_id == request.user:
            return function(request, *args, **kwargs)
        else:
            raise PermissionDenied
    return wrap
