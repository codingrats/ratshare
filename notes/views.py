from django.shortcuts import render, redirect
from django.template.loader import render_to_string
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib import messages
from notes.models import Note
from notes.forms import NoteForm
from notes.decorators import user_is_note_author
from datetime import datetime


@login_required
def note_list(request):
    template = 'notes/note-list.html'
    notes_list = Note.objects.filter(user_id=request.user.id).order_by('-add_date')
    if not notes_list:
        messages.info(request, 'Note list is empty')
    page = request.GET.get('page', 1)
    paginator = Paginator(notes_list, 5)
    try:
        notes = paginator.page(page)
    except PageNotAnInteger:
        notes = paginator.page(1)
    except EmptyPage:
        notes = paginator.page(paginator.num_pages)
    context = {'notes': notes}
    return render(request, template, context)


def save_note_form(request, form, template, edit):
    data = dict()
    data['edit'] = edit
    if request.method == 'POST':
        if form.is_valid:
            data['form_is_valid'] = True
            note = form.save(commit=False)
            if edit == 0:
                note.user_id = request.user
                messages.success(request, 'Note "{0}" successfully added'.format(note.title))
            if edit == 1:
                note.edit_date = datetime.now()
                messages.warning(request, 'Note "{0}" was modified'.format(note.title))
            note.save()
        else:
            data['form_is_valid'] = False
    context = {'form': form}
    data['html_form'] = render_to_string(template, context, request)
    return JsonResponse(data)


@login_required
def add_note(request):
    template = 'notes/note-create.html'
    if request.method == 'POST':
        form = NoteForm(request.POST)
    else:
        form = NoteForm()
    return save_note_form(request, form, template, edit=0)


@login_required
@user_is_note_author
def edit_note(request, pk):
    template = 'notes/note-edit.html'
    note = Note.objects.get(pk=pk)
    if request.method == 'POST':
        form = NoteForm(request.POST, instance=note)
    else:
        form = NoteForm(instance=note)
    return save_note_form(request, form, template, edit=1)


@login_required
@user_is_note_author
def delete_note(request, pk):
    note = Note.objects.get(pk=pk)
    note.delete()
    messages.success(request, 'Note "{0}" successfully deleted'.format(note.title))
    return redirect('note_list')


@login_required
def delete_selected_notes(request):
    notes = request.GET.getlist("notes[]")
    for note in notes:
        Note.objects.get(id=note).delete()
    return redirect('note_list')
