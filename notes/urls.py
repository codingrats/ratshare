from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.note_list, name='note_list'),
    url(r'^addnote/$', views.add_note, name='add_note'),
    url(r'^(?P<pk>[0-9]+)/editnote/$', views.edit_note, name='edit_note'),
    url(r'^(?P<pk>[0-9]+)/deletenote/$', views.delete_note, name='delete_note'),
    url(r'^deleteselected/$', views.delete_selected_notes, name='delete_selected'),
]
