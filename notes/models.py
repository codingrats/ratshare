from django.db import models
from django.contrib.auth.models import User


STATUS = {
    ('default', 'Default'),
    ('success', 'Green'),
    ('info', 'Blue'),
    ('warning', 'Yellow'),
    ('danger', 'Red')
}


class Note(models.Model):
    user_id = models.ForeignKey(User, blank=True, null=True)
    title = models.CharField(max_length=255)
    content = models.TextField(max_length=2000, blank=True)
    status = models.CharField(max_length=255, choices=STATUS, default='default')
    add_date = models.DateTimeField(auto_now_add=True)
    edit_date = models.DateTimeField(blank=True, null=True)
