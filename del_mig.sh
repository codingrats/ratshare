#!/bin/sh
echo "Starting ..."

echo ">> Deleting old migrations"
find . -path "*/migrations/*.py" -not -name "__init__.py" -delete
find . -path "*/migrations/*.pyc"  -delete

echo "Do you want run makemigrations? y/n"
read ans

if [ "$ans" = "y" ] 
	then
	  python manage.py makemigrations
	  echo "Do you want migrate? y/n"
	  read ans
	  if [ "$ans" = "y" ]
            then
		python manage.py migrate
	  fi
fi
	

echo ">> Done"
