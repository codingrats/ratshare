from django.conf.urls import url
from django.contrib.auth import views
from accounts.forms import MyAuthenticationForm

urlpatterns = [
    url(r'^login/$', views.login, {'template_name': 'accounts/login.html',
                                   'authentication_form': MyAuthenticationForm}, name='login'),
    url(r'^logout/$', views.logout, {'next_page': '/'}, name='logout'),
]
