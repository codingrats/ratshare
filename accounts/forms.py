from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit, Layout, Field
from django.contrib.auth.forms import AuthenticationForm


class MyAuthenticationForm(AuthenticationForm):
    helper = FormHelper()
    layout = helper.layout = Layout()
    layout.append(Field('username', placeholder='Username'))
    layout.append(Field('password', placeholder='Password'))
    helper.form_show_labels = False
    helper.add_input(Submit('submit', 'submit'))
