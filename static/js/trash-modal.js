$(function () {

    var loadForm = function () {
        var btn = $(this);
        $.ajax({
            url: btn.attr("data-url"),
            type: 'get',
            dataType: 'json',
            beforeSend: function () {
                $("#modal-trash").modal('show');
            },
            success: function (data) {
                $("#modal-trash .modal-content").html(data.html_table);
            }
        })
    };

    var returnFile = function () {
        var btn = $(this);
        $.ajax({
            url: btn.attr("data-url"),
            type: 'get',
            dataType: 'json',
            success: function (data) {
                $("#modal-trash .modal-content").html(data.html_table);
            }
        })
    };

    var deleteFile = function () {
        var btn = $(this);
        $.ajax({
            url: btn.attr("data-url"),
            type: 'get',
            dataType: 'json',
            success: function (data) {
                $("#modal-trash .modal-content").html(data.html_table);
            }
        })
    };

    $(".trash-list").click(loadForm);
    $(".return-file").on("click", returnFile);
    $(".delete-file").on("click", deleteFile);
});