$(function () {
    var loadForm = function () {
        $.ajax({
            url: $(this).attr("data-url"),
            type: 'get',
            dataType: 'json',
            beforeSend: function () {
                $("#modal-note .modal-content").html("");
                $("#modal-note").modal("show");
            },
            success: function (data) {
                $("#modal-note .modal-content").html(data.html_form);
            }
        })
    };

    var saveForm = function () {
        $.ajax({
            url: $(this).attr("action"),
            data: $(this).serialize(),
            type: $(this).attr("method"),
            dataType: 'json',
            success: function (data) {
                if (data.form_is_valid) {
                    $("#modal-note").modal("hide").on("hidden.bs.modal", function () {
                        if (data.edit === 0) {
                            window.location.replace("?page=1/");
                        }
                        else {
                            location.reload();
                        }
                    });
                }
                else {
                    $("#modal-note .modal-content").html(data.html_form);
                }
            }
        });
        return false;
    };

    $(".js-create-note").click(loadForm);
    $("#modal-note").on("submit", ".js-note-create-form", saveForm);

    $(".js-update-note").on("click", loadForm);
    $("#modal-note").on("submit", ".js-note-update-form", saveForm);

});